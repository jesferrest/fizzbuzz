<?php

require_once '../lib/functions.php';
 
class FizzbuzzTest extends PHPUnit_Framework_TestCase {
 
    public function testFizzbuzz() {
        $this->assertEquals(2, fizzbuzz(2));
        $this->assertEquals('fizz', fizzbuzz(3));
        $this->assertEquals(4, fizzbuzz(4));
        $this->assertEquals('buzz', fizzbuzz(5));
        $this->assertEquals('fizz', fizzbuzz(6));
        $this->assertEquals('bizz', fizzbuzz(7));
        $this->assertEquals(8, fizzbuzz(8));
        $this->assertEquals('fizz', fizzbuzz(9));
        $this->assertEquals('buzz', fizzbuzz(10));
        $this->assertEquals(11, fizzbuzz(11));
        $this->assertEquals('fizz', fizzbuzz(12));
        $this->assertEquals('fizz', fizzbuzz(13));
        $this->assertEquals(14, fizzbuzz(14));
        $this->assertEquals('fizzbuzz', fizzbuzz(15));
        $this->assertEquals(16, fizzbuzz(16));
        $this->assertEquals('bizz', fizzbuzz(17));
        $this->assertEquals('fizz', fizzbuzz(18));
        $this->assertEquals(19, fizzbuzz(19));
        $this->assertEquals('buzz', fizzbuzz(20));
        $this->assertEquals('fizzbuzz', fizzbuzz(30));
        $this->assertEquals('fizzbuzzbizz', fizzbuzz(34));
        $this->assertEquals('fizzbuzz', fizzbuzz(45));
        $this->assertEquals('fizzbizz', fizzbuzz(57));
        $this->assertEquals('fizzbuzz', fizzbuzz(60));
        $this->assertEquals('fizzbizz', fizzbuzz(69));
        $this->assertEquals('buzz', fizzbuzz(100));
    }
    
}
