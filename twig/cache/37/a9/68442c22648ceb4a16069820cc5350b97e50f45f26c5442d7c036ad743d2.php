<?php

/* index.html */
class __TwigTemplate_37a968442c22648ceb4a16069820cc5350b97e50f45f26c5442d7c036ad743d2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <title>Fizzbuzz</title>
        <meta charset=\"UTF-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <link rel=\"stylesheet\" href=\"./css/reset.css\" />
        <link rel=\"stylesheet\" href=\"./css/style.css\" />
    </head>
    <body>
        <div id=\"content\">
            <form action=\"fizzbuzz.php\" method=\"post\">
                <span class=\"text1\">Enter a number: </span>
                <input type=\"number\" name=\"number\" />
                <br /><br />
                <input class=\"button1\" type=\"submit\" value=\"ENTER\" />
            </form>
        </div>
    </body>
</html>
";
    }

    public function getTemplateName()
    {
        return "index.html";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
