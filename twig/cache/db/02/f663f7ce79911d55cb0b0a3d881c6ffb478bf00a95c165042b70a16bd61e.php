<?php

/* result.html */
class __TwigTemplate_db02f663f7ce79911d55cb0b0a3d881c6ffb478bf00a95c165042b70a16bd61e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <title>Fizzbuzz</title>
        <meta charset=\"UTF-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <link rel=\"stylesheet\" href=\"./css/reset.css\" />
        <link rel=\"stylesheet\" href=\"./css/style.css\" />
    </head>
    <body>
        <div id=\"content\">
            <span class=\"text1\">The result is ";
        // line 12
        echo twig_escape_filter($this->env, (isset($context["result"]) ? $context["result"] : null), "html", null, true);
        echo "</span>
            <br /><br />
            <a href=\"index.php\" >Enter new number</a>
        </div>
    </body>
</html>
";
    }

    public function getTemplateName()
    {
        return "result.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  32 => 12,  19 => 1,);
    }
}
