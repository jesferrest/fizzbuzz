<?php

function divisible($number, $divisor){
    if(0 == $number % $divisor){
        return true;
    }
    return false;
}

function contiene($number, $digito){
    while($number > 1){
        $ult_dig = $number % 10;
        if($ult_dig == $digito){
            return true;
        }
        $number = $number / 10;
    }
    return false;
}

function is_fizz($number){
    if(divisible($number, 3) || contiene($number, 3)){
        return true;
    }
    return false;
}

function is_buzz($number){
    if(34 == $number || divisible($number, 5)){
        return true;
    }
    return false;
}

function is_bizz($number){
    if(34 == $number || 69 == $number || contiene($number, 7)){
        return true;
    }
    return false;
}

function fizzbuzz($number){
    $cadena = '';
    
    if(is_fizz($number)){
        $cadena .= 'fizz';
    }
    if(is_buzz($number)){
        $cadena .= 'buzz';
    }
    if(is_bizz($number)){
        $cadena .= 'bizz';
    }
    
    if(empty($cadena)){
        return $number;
    }
    return $cadena;
}
