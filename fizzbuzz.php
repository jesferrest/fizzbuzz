<?php

if(isset($_POST['number']) && is_numeric($_POST['number'])){
    include('lib/functions.php');
    
    $result = fizzbuzz($_POST['number']);
}
else{
    header("location: index.php");
    exit();
}

require_once 'twig/lib/Twig/Autoloader.php';
Twig_Autoloader::register();

$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader, array(
    'cache' => 'twig/cache',
));

echo $twig->render('result.html', array('result' => $result));
